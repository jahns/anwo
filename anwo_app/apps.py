from django.apps import AppConfig


class AnwoAppConfig(AppConfig):
    name = 'anwo_app'
