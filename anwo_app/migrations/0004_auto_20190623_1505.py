# Generated by Django 2.2.2 on 2019-06-23 19:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('anwo_app', '0003_auto_20190623_1505'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='tipo',
            field=models.CharField(choices=[('S', 'Split'), ('P', 'Portatil')], max_length=150),
        ),
    ]
