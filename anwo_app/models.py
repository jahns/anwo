from django.db import models

# Create your models here.
class Marca(models.Model):
    descripcion = models.CharField(max_length = 150, unique = True)

    def __str__(self):
        return "marca"

class Proveedor(models.Model):
    nombre = models.CharField(max_length = 150)
    telefono = models.BigIntegerField()
    direccion = models.CharField(max_length = 250)
    pagina_web = models.CharField(max_length = 150)

    def __str__(self):
        return "proveedor"

class Producto(models.Model):
    imagesrc = models.CharField(max_length = 150, default = "http://localhost:9000/media/producto/producto.jpg")
    descripcion = models.CharField(max_length = 150)
    modelo = models.CharField(max_length = 150, unique = True)
    precio = models.BigIntegerField()
    cantidad = models.BigIntegerField()
    tipos_producto = {
        ('S', 'Split'),
        ('P', 'Portatil'),
    }
    tipo = models.CharField(max_length = 150, choices = tipos_producto)
    marca = models.ForeignKey(Marca, on_delete = models.CASCADE)
    proveedor = models.ForeignKey(Proveedor, on_delete = models.CASCADE)

    def __str__(self):
        return "producto"