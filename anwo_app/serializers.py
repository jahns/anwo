from rest_framework import serializers
from .models import Marca, Proveedor, Producto

# Create your serializers here.
class MarcaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Marca
        fields = (
            'url',
            'id',
            'descripcion'
        )

class ProveedorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Proveedor
        fields = (
            'url',
            'id',
            'nombre',
            'telefono',
            'direccion',
            'pagina_web'
        )

class ProductoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Producto
        fields = (
            'url',
            'id',
            'imagesrc',
            'descripcion',
            'modelo',
            'precio',
            'cantidad',
            'tipo',
            'marca',
            'proveedor'
        )