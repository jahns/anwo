from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

#django rest framework
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'marca', views.MarcaView)
router.register(r'producto', views.ProductoView)
router.register(r'proveedor', views.ProveedorView)

urlpatterns = [
    path('', views.index, name='index'),
    #rest api
    path('api/', include(router.urls)),
    path('oauth/', include('oauth2_provider.urls', namespace='oauth2_provider')),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)