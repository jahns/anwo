from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from .models import Marca, Proveedor, Producto

#rest framework
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from .serializers import MarcaSerializer, ProveedorSerializer, ProductoSerializer

#oauth2
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope

# Create your views here.
def index(request):
    return render(
        request,
        'index.html',
        {}
    )

# Create your rest views here.
class MarcaView(viewsets.ModelViewSet):
    queryset = Marca.objects.all()
    serializer_class = MarcaSerializer
    permission_classes = (
        #AllowAny,
        TokenHasReadWriteScope,
    )

class ProveedorView(viewsets.ModelViewSet):
    queryset = Proveedor.objects.all()
    serializer_class = ProveedorSerializer
    permission_classes = (
        #AllowAny,
        TokenHasReadWriteScope,
    )

class ProductoView(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    permission_classes = (
        #AllowAny,
        TokenHasReadWriteScope,
    )